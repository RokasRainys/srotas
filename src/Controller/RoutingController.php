<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RoutingController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        return $this->render('routing/index.html.twig', [
            'controller_name' => 'RoutingController',
        ]);
    }

    /**
     * @Route("/parts", name="parts")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function parts()
    {
        return $this->render('routing/parts.html.twig');
    }

    /**
     * @Route("/cars", name="cars")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function car()
    {
        return $this->render('routing/cars.html.twig', [
            'fill' => [
                'color' => 'FFD9AB',
            ]
        ]);
    }

    /**
     * @Route("/video", name="video")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function video()
    {
        return $this->render('routing/video.html.twig');
    }

    /**
     * @Route("/part", name="part")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function part()
    {
        return $this->render('routing/part.html.twig');
    }

    /**
     * @Route("/completed", name="completed")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function completed()
    {
        return $this->render('routing/completed.html.twig');
    }

    /**
     * @Route("/login", name="login")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login()
    {
        return $this->render('routing/login.html.twig');
    }

    /**
     * @Route("/profile", name="profile")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profile()
    {
        return $this->render('routing/profile.html.twig');
    }
}
